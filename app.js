//DOM
const prodList = document.getElementById("ProdList");
const productFeatures = document.getElementById("ProdFeatures");
const prodName = document.getElementById("ProdName");
const prodImage = document.getElementById("ProdImage");
const prodDiscription = document.getElementById("ProdDiscription");
const prodPrice = document.getElementById("ProdPrice");
const buyButton = document.getElementById("BuyButton");

const bankBalance = document.getElementById("BankBalance");
const loanButton = document.getElementById("LoanButton");
const payCheck = document.getElementById("Paycheck");
const depositButton = document.getElementById("DepositButton");
const workButton = document.getElementById("WorkButton");

const loanElement = document.getElementById("LoanAmounth");
const loanInfo = document.getElementById("LoanInfo")
const payBackLoanButton = document.getElementById("PayBackLoanButton")

//member variables
let products = [];
let balance = 0;
let salary = 0;
let currentProductPrice = 0;
let loanAmounth = 0

//to make sure everything is set to 0
updateBalance();

//api call

fetch("https://noroff-komputer-store-api.herokuapp.com/computers")
    .then(response => response.json())
    .then(data => products = data)
    .then(products => addProductsToList(products));

const addProductsToList = (products) => {
    products.forEach(x => addProductToList(x))
}

const addProductToList = (product) => {
    const prodElement = document.createElement("option");
    prodElement.value = product.id;
    prodElement.innerText = product.title;
    prodList.appendChild(prodElement);
}



function updateBalance() {
    bankBalance.innerText = "€ " + balance;
    payCheck.innerText = "€ " + salary;
    loanElement.innerText = "€ " + loanAmounth;
}
function changeLoan(loan){
    loanAmounth += loan;
    balance += loanAmounth
    updateBalance();
    loanInfo.style.visibility = "visible";
    loanElement.style.visibility = "visible";
    payBackLoanButton.style.visibility = "visible";


}

//Event handlers
const handleProductListChange = e => {
    const selectedProduct = products[e.target.selectedIndex];
    currentProductPrice = selectedProduct.price
    prodPrice.innerText = "€ " + selectedProduct.price;
    productFeatures.innerText = selectedProduct.description;
    prodDiscription.innerText = selectedProduct.specs;
    prodImage.src = "https://noroff-komputer-store-api.herokuapp.com/" + selectedProduct.image;
    prodName.innerText = selectedProduct.title;
}
const handleWorkButtonClick = e => {
    salary += 200-(loanAmounth*0.1);
    updateBalance();
}
const handleAddButton = e => {
    //console.log(currentProductPrice +""+ balance)
    if (currentProductPrice <= balance && currentProductPrice !== 0){
        alert("Thank you for your purchase")
        balance -= currentProductPrice;
        updateBalance();
    }
    else
        alert("FAIL")
}
const handleDepositButton = e => {
    balance += salary;
    salary = 0;
    updateBalance();
}
const handleLoanButton = e => {
    if(loanAmounth > 0)
        alert("you can't get another loan")
    else{
        try{
            tempLoanAmounth = parseInt(window.prompt("How much do you need?"))
        }
        catch(err){
            alert("Please enter a valid amounth");
            return;
        }
        if(tempLoanAmounth >= 2000 || tempLoanAmounth > (2*balance) || !tempLoanAmounth){
            alert("Can't grand loan, you will never be able to pay this off")
        }
        else{
            changeLoan(tempLoanAmounth);
        }
    }
}
const handlePayBackLoanButton = e => {
    if(salary>=loanAmounth){
        salary -= loanAmounth;
        loanAmounth = 0;
        updateBalance();
    }
    else
        alert("You can't pay you debt")
}

//Event listerners
depositButton.addEventListener("click", handleDepositButton)
prodList.addEventListener("change", handleProductListChange)
workButton.addEventListener("click", handleWorkButtonClick)
buyButton.addEventListener("click", handleAddButton)
loanButton.addEventListener("click", handleLoanButton)
payBackLoanButton.addEventListener("click", handlePayBackLoanButton)

